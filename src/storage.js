const storage = {
  pokemons: [ ],
};

export const saveStorage = () => {
  localStorage.setItem('pokemons', JSON.stringify(storage.pokemons));
}

export const storeStorage = () => { 
  if (localStorage.getItem('pokemons')) {
    storage.pokemons = JSON.parse(localStorage.getItem('pokemons'));
  }
}
storeStorage();

export default storage;

export const stockCount = (name) => {
  let pokemon = storage.pokemons.find(pokemon => pokemon.name === name);
  if (!pokemon || pokemon.histories.length === 0) return 0;
  return pokemon.histories.map(history => parseInt(history.increase[0]) + parseInt(history.increase[1]*12)).reduce((total, i) => total+=parseInt(i));
}

export const stockHistories = (name) => {
  let pokemon = storage.pokemons.find(pokemon => pokemon.name === name);
  if (!pokemon || pokemon.histories.length === 0) return [];
  pokemon.histories.sort((a, b) => b.id - a.id);
  pokemon.histories = pokemon.histories.map((history, i) => {
    history.stock_before = pokemon.histories.slice(i, pokemon.histories.length);
    history.stock_before = history.stock_before.length > 0 ? history.stock_before.map(history => countHistory(history)).reduce((total, i) => total+=i) : [];
    return history;
  })
  return pokemon.histories;
}

export const countHistory = (history) => {
  return parseInt(history.increase[0]) + parseInt(history.increase[1])*12;
}
