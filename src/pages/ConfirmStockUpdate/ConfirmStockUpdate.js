import React from 'react';

import storage, {countHistory, stockCount, saveStorage } from './../../storage';

// setup router
import { useParams, useNavigate } from "react-router-dom";

// Material UI
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import AddIcon from '@mui/icons-material/Add';
import Hidden from '@mui/material/Hidden';
import DetailPokemonForm from './../DetailPokemon/DetailPokemonForm';
import AppBarMobileCancel from './../../components/AppBar/AppBarMobileCancel'
import { numberWithCommas } from './../../helper';

export default function ConfirmStockUpdate() {

  let [note, setNote] = React.useState('');
  let [openForm, setOpenForm] = React.useState(false);

  let { name, id } = useParams();
  let navigate = useNavigate();

  let pokemon = storage.pokemons.find(pokemon => pokemon.name === name);
  let history = pokemon.histories.find(history => history.id == id);

  let storeHistory = () => {
    history.note = note;
    saveStorage();
    navigate(`/stock-pokemon/${name}`);
  }

  let cancelHistory = () => {
    pokemon.histories.splice(pokemon.histories.findIndex(history => history.id == id), 1);
    navigate(`/stock-pokemon/${name}`);
  }

  return (
    <React.Fragment>

      <Hidden mdUp>
        <AppBarMobileCancel name={name} /> 
      </Hidden>

      <Container maxWidth="lg">

        <Typography variant="h3" sx={{ my: '2rem' }} component="div">Konfirmasi update stok</Typography>

        <Grid container spacing={2} alignItems="center" sx={{ mb: '3rem' }}>
          <Grid item xs={12}>
            <Box sx={{ my: '1rem' }}>
              <Typography variant="body2">Selisih</Typography>
              <Typography variant="h4" component="div"> <AddIcon/>{numberWithCommas(countHistory(history))} pcs </Typography>
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box sx={{ my: '1rem' }}>
              <Typography variant="body2">Di sistem</Typography>
              <Typography variant="h5" component="div">{numberWithCommas(stockCount(name) - countHistory(history))} pcs </Typography>
            </Box>
          </Grid>
          <Grid item xs={6}>
          <Box sx={{ my: '1rem' }}>
            <Typography variant="body2">Hasil update stok</Typography>
            <Typography variant="h5" component="div"> <ArrowForwardIcon/> {numberWithCommas(stockCount(name))} pcs </Typography>
          </Box>
          </Grid>
        </Grid>

        <Hidden mdDown>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080', fontWeight: '700' }}>
            <Grid item xs={4}>
              Keterangan
            </Grid>
            <Grid item xs={5}>
              Detail
            </Grid>
            <Grid item xs={2} sx={{ textAlign: 'right' }}>
              Jumlah
            </Grid>
          </Grid>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080' }}>
            <Grid item xs={4}>
              <Typography variant="body1" color="teal" sx={{fontWeight: '500'}}>Hasil update stok</Typography>
            </Grid>
            <Grid item xs={5}>
              {history.increase[0]} pcs, {history.increase[1]} lusin (12x)
            </Grid>
            <Grid item xs={2} sx={{ textAlign: 'right' }}>
              {numberWithCommas(stockCount(name))} pcs
            </Grid>
            <Grid item xs={1}>
              <IconButton onClick={() => setOpenForm(true)}>
                <EditIcon />
              </IconButton>
            </Grid>
          </Grid>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '3rem', fontWeight: '700' }}>
            <Grid item xs={9}>
              <Typography variant="body1" color="teal" sx={{fontWeight: '500'}}>Total hasil stok opname</Typography>
            </Grid>
            <Grid item xs={2} sx={{ textAlign: 'right' }}>
              {numberWithCommas(stockCount(name))} pcs
            </Grid>
          </Grid>
        </Hidden>

        <Hidden mdUp>
          <Grid container alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080', fontWeight: '700' }}>
            <Grid item xs={6}>
              Keterangan
            </Grid>
            <Grid item xs={4} sx={{ textAlign: 'right' }}>
              Jumlah
            </Grid>
          </Grid>

          <Grid container alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080' }}>
            <Grid item xs={7}>

              <Typography variant="body1" color="teal" sx={{fontWeight: '500'}}>Hasil update stok</Typography>
              <Typography variant="body2">{history.increase[0]} pcs, {history.increase[1]} lusin (12x)</Typography>
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              {numberWithCommas(stockCount(name))} pcs
            </Grid>
            <Grid item xs={2} sx={{ textAlign: 'right' }}>
              <IconButton onClick={() => setOpenForm(true)}>
                <EditIcon />
              </IconButton>
            </Grid>
          </Grid>

          <Grid container alignItems="center" sx={{ mb: '3rem', fontWeight: '700' }}>
            <Grid item xs={7}>
              <Typography variant="body1" sx={{fontWeight: '500'}}>Total hasil stok opname</Typography>
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              {numberWithCommas(stockCount(name))} pcs
            </Grid>
          </Grid>
        </Hidden>

        <TextField
          size="sm"
          label="Catatan"
          type="text"
          fullWidth
          variant="outlined"
          value={note}
          onChange={(event) => { setNote(event.target.value) }}
          sx={{mt: '1rem'}}
        />

        <Box sx={{ my: '2rem', textAlign: 'right' }}>
          <Button color="success" onClick={storeHistory} variant="contained" sx={{ mr: 1 }}>Simpan</Button>

          <Hidden mdDown>
          <Button onClick={cancelHistory}>Batal</Button>
          </Hidden>
        </Box>

        <DetailPokemonForm name={name} stockCount={stockCount(name)} form={openForm} closeForm={() => setOpenForm(false)} history={history}/>

        </Container>


    </React.Fragment>
  );
}
