import React from 'react';
import {stockCount, stockHistories} from './../../storage';

// setup router
import { useParams, useNavigate } from "react-router-dom";

// Material UI
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Hidden from '@mui/material/Hidden';
import { numberWithCommas } from './../../helper';

import AppBar from './../../components/AppBar/AppBar'
import AppBarMobile from './../../components/AppBar/AppBarMobile'

import DetailPokemonHistory from './DetailPokemonHistory';
import DetailPokemonForm from './DetailPokemonForm';

export default function DetailPokemon() {

  let { name } = useParams();
  let [openForm, setOpenForm] = React.useState(false);
  let navigate = useNavigate();

  return (
    <React.Fragment>

      <Hidden mdDown>
        <AppBar openForm={() => setOpenForm(true)}/> 
      </Hidden>
      <Hidden mdUp>
        <AppBarMobile/> 
      </Hidden>

      <Container maxWidth="lg">

        <Box sx={{ mt: '1rem', mb: '2rem' }}>
        <Typography variant="h3" component="div" sx={{ textTransform: 'capitalize' }}>{name}</Typography>
        </Box>

        <Hidden mdUp>
        <Button color="success" variant="contained" onClick={() => setOpenForm(true)}>Update Stock</Button>
        </Hidden>

        <Box sx={{ my: '2rem' }}>
          <Typography variant="body2">Sisa Stok</Typography>
          <Typography variant="h4" component="div">{numberWithCommas(stockCount(name))} pcs</Typography>
        </Box>

        <Box sx={{ my: '1rem' }}>
          <Typography variant="h6">Riwayat Stok</Typography>
          <Typography variant="body2">Satuan dalam pcs</Typography>
        </Box>

        <DetailPokemonHistory histories={stockHistories(name)} />
        <DetailPokemonForm name={name} stockCount={stockCount(name)} form={openForm} closeForm={() => setOpenForm(false)}/>

      </Container>
    </React.Fragment>
  );
}
