import * as React from 'react';
import { useNavigate } from "react-router-dom";
import {countHistory} from './../../storage';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableHead from '@mui/material/TableHead';
import Hidden from '@mui/material/Hidden';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { numberWithCommas } from './../../helper';

export default function DetailPokemonHistory({ histories }) {

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  let date = '', header = '';

  return (
    <React.Fragment>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>

        <Hidden mdDown>
          <TableContainer>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Waktu</TableCell>
                  <TableCell>Kegiatan</TableCell>
                  <TableCell>Catatan</TableCell>
                  <TableCell align="right">Jumlah</TableCell>
                  <TableCell align="right">Stok</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {histories.concat([{ id: 0, description: 'Stok Awal', datetime: 'Apr 26, 2022 00:00 AM', increase: [0, 0], stock_before: 0 }]).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, i) => (
                  <TableRow
                    key={row.id}
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    sx={{ cursor: 'pointer' }}
                  >
                    <TableCell component="th" scope="row">
                      {row.datetime}
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Typography variant="body2" color="teal" sx={{fontWeight: '400'}}>{row.description}</Typography>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.note}
                    </TableCell>
                    <TableCell align="right">
                      <Typography variant="body2" color="green" sx={{fontWeight: '400'}}>+{numberWithCommas(countHistory(row))}</Typography>
                    </TableCell>
                    <TableCell align="right">{numberWithCommas(row.stock_before)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={histories.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Hidden>

      </Paper>
      
      <Hidden mdUp>
          <Box sx={{mt: '2rem'}}>
          {histories.concat([{ id: 0, description: 'Stok Awal', datetime: 'Apr 26, 2022 00:00 AM', increase: [0, 0], stock_before: 0 }]).map((row, i) => {
              if (date !== row.datetime.split('').slice(0, 12).join('')) {
                date = row.datetime.split('').slice(0, 12).join('');
                return (
                  <React.Fragment>
                    <Grid container key={row.id} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080', fontWeight: '700' }}>
                      <Grid item xs={6}>
                        { date }
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        Jml
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        Stok
                      </Grid>
                    </Grid>
                    <Grid container key={row.id} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080' }}>
                      <Grid item xs={6}>
                        <Typography variant="body2">{ row.datetime.split('').slice(12).join('') }</Typography>
                        <Typography variant="body1" color="teal" sx={{fontWeight: '400'}}>{ row.description }</Typography>
                        <Typography variant="body2">{ row.note }</Typography>
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        <Typography variant="body1" color="green" sx={{fontWeight: '400'}}>+{ numberWithCommas(countHistory(row)) }</Typography>
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        {numberWithCommas(row.stock_before)}
                      </Grid>
                    </Grid>
                  </React.Fragment>
                );
              } else {
                  date = row.datetime.split('').slice(0, 12).join('');
                  return (
                    <Grid container key={row.id} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080' }}>
                      <Grid item xs={6}>
                        <Typography variant="body2">{ row.datetime.split('').slice(12).join('') }</Typography>
                        <Typography variant="body1" color="teal" sx={{fontWeight: '400'}}>{ row.description }</Typography>
                        <Typography variant="body2">{ row.note }</Typography>
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        <Typography variant="body1" color="green" sx={{fontWeight: '400'}}>+{ numberWithCommas(countHistory(row)) }</Typography>
                      </Grid>
                      <Grid item xs={3} sx={{ textAlign: 'right' }}>
                        {numberWithCommas(row.stock_before)}
                      </Grid>
                    </Grid>
                  )
              }
          })}
          </Box>
      </Hidden>
    </React.Fragment>
  );
}
