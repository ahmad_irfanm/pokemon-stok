import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import storage, {countHistory} from './../../storage'
import { numberWithCommas } from './../../helper';
import moment from 'moment';

export default function DetailPokemonForm({ form, name, closeForm, stockCount, history }) {

  let [pcs, setPcs] = React.useState(0);
  let [lusin, setLusin] = React.useState(0);
  let navigate = useNavigate();

  React.useEffect(() => {
    if (history) {
      setPcs(history.increase[0]);
      setLusin(history.increase[1]);
    }
  }, [])

  let saveForm = () => {
    let pokemon  = storage.pokemons.find(pokemon => pokemon.name === name), historyId;

    // new pokemon
    if (!pokemon) {
      pokemon = { name, histories: [] };
      storage.pokemons.push(pokemon);
    }

    if (!history) {
      historyId = pokemon.histories.length + 1;
      pokemon.histories.push({
        id: historyId,
        datetime: moment().format('lll'),
        description: 'Update stock',
        increase: [parseInt(pcs), parseInt(lusin)],
      });
    } else {
      history.increase = [parseInt(pcs), parseInt(lusin)];
      historyId = history.id;
    }

    navigate(`/stock-pokemon/${name}/confirm-stock-update/${historyId}`);
    closeForm();
  }

  return (
    <div>
      <Dialog open={form} onClose={closeForm} fullWidth={false}>
        <DialogTitle sx={{ textAlign: 'center', fontWeight: 'bold', mb: '-1rem' }}>Update stock</DialogTitle>
        <DialogContent>
          <DialogContentText sx={{ textAlign: 'center', mb: '3rem' }}>
            Masukan jumlah stok yang tersedia di rak saat ini.
          </DialogContentText>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080', fontWeight: 'bold' }}>
            <Grid item xs={5}>
              Kemasan
            </Grid>
            <Grid item xs={4} sx={{ textAlign: 'center' }}>
              Jumlah
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              Stok
            </Grid>
          </Grid>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '1rem', pb: '1rem', borderBottom: '1px solid #808080' }}>
            <Grid item xs={4}>
              Pcs
            </Grid>
            <Grid item xs={5}>
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={4} sx={{ textAlign: 'right' }}>
                  1 x
                </Grid>
                <Grid item xs={7}>
                  <TextField
                    autoFocus
                    size="sm"
                    label="Stok"
                    type="number"
                    fullWidth
                    variant="outlined"
                    value={pcs}
                    inputProps={{ inputMode: 'numeric' }}
                    onChange={(event) => { setPcs(event.target.value) }}
                  />
                </Grid>
                <Grid item xs={1}>
                  =
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              {numberWithCommas(pcs)}
            </Grid>

            <Grid item xs={4}>
              Lusin
            </Grid>
            <Grid item xs={5}>
              <Grid container spacing={1} alignItems="center">
                <Grid item xs={4} sx={{ textAlign: 'right' }}>
                  12 x
                </Grid>
                <Grid item xs={7}>
                  <TextField
                    size="sm"
                    label="Stok"
                    type="number"
                    fullWidth
                    variant="outlined"
                    value={lusin}
                    inputProps={{ inputMode: 'numeric' }}
                    onChange={(event) => { setLusin(event.target.value) }}
                  />
                </Grid>
                <Grid item xs={1}>
                  =
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              {numberWithCommas(lusin*12)}
            </Grid>
          </Grid>

          <Grid container spacing={2} alignItems="center" sx={{ mb: '1rem', fontWeight: 'bold' }}>
            <Grid item xs={9}>
              Total (stok dalam pcs)
            </Grid>
            <Grid item xs={3} sx={{ textAlign: 'right' }}>
              {numberWithCommas(stockCount + parseInt(pcs)+parseInt(lusin*12) - (history ? countHistory(history) : 0))}
            </Grid>
          </Grid>

        </DialogContent>
        <DialogActions sx={{ p: '1rem' }}>
          <Button color="success" variant="contained" onClick={saveForm} sx={{ mr: 1 }}>Simpan</Button>
          <Button onClick={closeForm}>Batal</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
