import React, { useEffect } from 'react';
import axios from 'axios';
import {stockCount} from './../../storage';

// Material UI
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

// Self Components
import StockPokemonTable from './StockPokemonTable';

export default function StockPokemon() {

  const [value, setValue] = React.useState('');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  let [pokemons, setPokemons] = React.useState([]);

  useEffect(() => {

    const fetchData = async () =>{
      try {
        const {data: response} = await axios.get('https://pokeapi.co/api/v2/pokemon');
        response.results.forEach(pokemon => {
          pokemon.stock = stockCount(pokemon.name);
        });
        setPokemons(response.results);
      } catch (error) {
        console.error(error.message);
      }
    }

    fetchData();

  }, []);


  return (
    <React.Fragment>
      <Container maxWidth="lg">
        <Typography variant="h3" sx={{ my: '2rem' }} component="div">Stok Pokemon</Typography>

        <TextField
          id="outlined-multiline-flexible"
          label="Search Pokemon"
          multiline
          maxRows={4}
          value={value}
          onChange={handleChange}
          fullWidth
          sx={{ mb: '2rem' }}
        />

        <StockPokemonTable pokemons={pokemons.filter(pokemon => pokemon.name.toLowerCase().indexOf(value.toLowerCase()) > -1)}/>
      </Container>
    </React.Fragment>
  );
}
