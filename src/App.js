import React from 'react';
import logo from './logo.svg';
import './App.css';

// setup router
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

// components
import Footer from './components/Footer/Footer';
import Box from '@mui/material/Box';

// setup pages
import pages from './pages.js';

import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from '@mui/material/styles';

let theme = createTheme({
  typography: {
    h3: {
      fontWeight: 'bold',
    },
    fontFamily: [
      '-apple-system',
      'Rubik',
      '"Segoe UI"',
      'sans-serif',
    ].join(','),
  },
});
theme = responsiveFontSizes(theme);


function App() {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <Box sx={{ minHeight: '100vh' }}>
          <Routes> { pages.map((page, key) => (<Route key={key} path={page.path} element={page.element}></Route>) ) } </Routes>
        </Box>

        <Footer/>
      </ThemeProvider>
    </Router>
  );
}

export default App;
