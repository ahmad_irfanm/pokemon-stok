import * as React from 'react';
import { useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Container from '@mui/material/Container';

export default function ButtonAppBar({ openForm }) {
  let navigate = useNavigate()
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="transparent">
        <Container maxWidth="lg">
        <Toolbar>
          <Button variant="inherit" startIcon={<ArrowBackIcon />} onClick={() => navigate('/')} sx={{ ml: '-2rem' }}>
            Stok Pokemon
          </Button>
          <Box sx={{ flexGrow: 1 }} />
          <Button color="success" variant="contained" onClick={openForm}>Update Stock</Button>
        </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}
