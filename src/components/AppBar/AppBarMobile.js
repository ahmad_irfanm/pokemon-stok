import * as React from 'react';
import { useNavigate } from "react-router-dom";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

export default function ButtonAppBarMobile() {
  let navigate = useNavigate()
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="transparent">
        <Container maxWidth="lg">
        <Toolbar>
          <IconButton variant="inherit" onClick={() => navigate('/')} sx={{ ml: '-2rem' }}>
            <ArrowBackIcon />
          </IconButton>
          <Box sx={{ flexGrow: 1 }} />
          <Typography variant="h6">
            Stok Pokemon
          </Typography>
          <Box sx={{ flexGrow: 1 }} />
        </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}
