import React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Box';
import Box from '@mui/material/Box';

export default function Footer() {

  return (
    <React.Fragment>
      <Container maxWidth="lg">
        <Box sx={{ textAlign: 'center', padding: '3rem' }}>
          <Typography variant="body2">Copyright &copy; 2022, Ahmad Irfan M.</Typography>
        </Box>
      </Container>
    </React.Fragment>
  );
}
