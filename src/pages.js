import StockPokemon from './pages/StockPokemon/StockPokemon';
import DetailPokemon from './pages/DetailPokemon/DetailPokemon';
import ConfirmStockUpdate from './pages/ConfirmStockUpdate/ConfirmStockUpdate';

export default [
  {
    path: '/',
    title: 'Stock Pokemon',
    name: 'stock-pokemon',
    element: <StockPokemon/>,
  },
  {
    path: '/stock-pokemon/:name',
    title: 'Detail Pokemon',
    name: 'detail-pokemon',
    element: <DetailPokemon/>,
  },
  {
    path: '/stock-pokemon/:name/confirm-stock-update/:id',
    title: 'Confirm Stock Update',
    name: 'confirm-stock-update',
    element: <ConfirmStockUpdate/>,
  },
];
